const nuxtssr = require('./functions/nuxtssr');
const functions = require('firebase-functions');

exports.ssrapp = functions.https.onRequest(nuxtssr)